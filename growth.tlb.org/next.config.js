
module.exports.experimental = {
  images: {
    unoptimized: true,
  }
};


async function exportPathMap(defaultPathMap, {dev, dir, outDir, distDir, buildId})
{
  return {
    //'/': { page: '/' },
    ...defaultPathMap
  };
}

module.exports.exportPathMap = exportPathMap;
module.exports.reactStrictMode = true;
