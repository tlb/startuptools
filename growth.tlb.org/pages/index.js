import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import GrowthView from '../lib/GrowthView.js'

export default function Home() {
  return (
    <GrowthView/>
  );
}
