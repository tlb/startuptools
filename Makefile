
dev.% : force
	cd $*; npx next dev

build.% : force
	cd $*; npx next build

export.% : build.%
	cd $*; npx next export

deploy: deploy.kingston

deploy.kingston :: export.growth.tlb.org
	rsync -a --delete-after growth.tlb.org/out/. kingston:/var/www/growth.tlb.org/.

config.kingston : deploy.kingston
	ssh kingston 'sudo nginx -s reload'

force:
